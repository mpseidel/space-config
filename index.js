"use strict";
let nconf = require('nconf');
let path = require('path');
exports.config = nconf.argv()
    .env()
    .file({ file: path.join(process.cwd(), '/config/secrets.json') });
//# sourceMappingURL=index.js.map