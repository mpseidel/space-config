"use strict";

let nconf = require('nconf');
let path = require('path');

export const config = nconf.argv()
  .env()
  .file({ file: path.join(process.cwd(), '/config/secrets.json') });
